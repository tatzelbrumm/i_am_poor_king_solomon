// Moebius star
// 2014 FEB 22 by Christoph Maier
// Copyleft:
// CERN OHL v1 or GNU GPL v3, whichever applies.

module immelmann(w, r, t, theta, phi)
{
	ww= w*10;
	hh= r+t;
	rr= 2*r;
	difference()
	{
		rotate(90, [1,0,0]) difference() 
		{
			cylinder(r= r+t/2, h= w, center= true);
			cylinder(r= r-t/2, h= w, center= true);
		}
		rotate(phi, [0,0,1]) rotate(theta, [0,1,0]) translate([0,-ww/2,0]) cube(size=[rr,ww,hh]);
		rotate(-phi, [0,0,1]) rotate(-theta, [0,1,0]) translate([0,-ww/2,-hh]) cube(size=[rr,ww,hh]);
	}
}

module turns(n, r, w, t, c, theta, phi)
{
	for(k= [0:n-1])
	{
		rotate(360*k/n, [0,0,1]) translate([-r, 0, 0]) immelmann(w= w, r= c, t= t, theta= theta, phi= phi);
	} 
} 

module linguini(n, r, w, t, c, phi, tilt, l)
{
	shift= r*sin(tilt);
	for(k= [0:n-1])
	{
		rotate(tilt+k*phi, [0,0,1]) translate([0,shift,0]) rotate(atan2(2*c,l),[0,1,0]) cube([l,w,t], center= true);
	}
}

module star(n, r, w, t, c)
{
	phi= 360*ceil(n/2-1)/n;
	tilt= (180-phi)/2;
	l= sqrt((r*r)*(pow(1-cos(phi),2)+pow(sin(phi),2))+pow(2*c,2));
	theta= atan2(2*c,l);
	union()
	{
		linguini(n= n, r= r, w= w, t= t, c= c, phi= phi, tilt= tilt, l= l);
		turns(n= n, r= r+c*sin(theta), w= w*cos(tilt), t= t, c= c, theta= theta, phi= tilt);
	}
}

// immelmann(w=8, r=10, t=1, theta=50, phi=20);
// turns(n= 5, r= 40, w=8, t= 1, c= 8);
// linguini(n= 5, r= 40, w= 8, t= 1, c= 8);
star(n= 8, r= 40, w=4, t= 1, c= 10);
